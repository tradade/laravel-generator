<?php

namespace InfyOm\Generator\Generators\Scaffold;

use Illuminate\Support\Str;
use InfyOm\Generator\Common\CommandData;
use InfyOm\Generator\Generators\BaseGenerator;
use InfyOm\Generator\Utils\FileUtil;
use InfyOm\Generator\Utils\GeneratorFieldsInputUtil;
use InfyOm\Generator\Utils\HTMLFieldGenerator;

class LangGenerator extends BaseGenerator
{
    /** @var CommandData */
    private $commandData;

    /** @var string */
    private $path;

    private $fileName;

    public function __construct(CommandData $commandData)
    {
        $this->commandData = $commandData;
        $this->path = $commandData->config->pathLang;
        $this->fileName = strtolower($this->commandData->modelName . '.php');
    }

    public function generate()
    {
        if (!file_exists($this->path)) {
            mkdir($this->path, 0755, true);
        }

        $this->commandData->commandComment("\nGenerating Lang...");

        $this->generateFile();

        $this->commandData->commandComment('Language file created ');
    }


    private function generateFile()
    {

        $fieldsStr = '<?php
            return [
        ';

        foreach ($this->commandData->fields as $field) {
            $singleFieldStr = "'" . $field->name . "' => '" . Str::title(str_replace('_', ' ', $field->name)) . "',";
            $fieldsStr .= $singleFieldStr . "\n\n";
        }
        $fieldsStr .= '];';

        FileUtil::createFile($this->path, $this->fileName, $fieldsStr);
        $this->commandData->commandInfo($this->fileName . ' created');
    }


    public function rollback()
    {
        if ($this->rollbackFile($this->path, $this->fileName)) {
            $this->commandData->commandComment($this->fileName . ' file deleted');
        }
    }
}
